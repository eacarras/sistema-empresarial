package logs;

import java.util.logging.Level;
import java.util.logging.Logger;

// This is our class for Logs we need to improve it later
public class DLog {
	
	private static Logger LOGGER;
	
	public static void info(String className, String message) {
		LOGGER = Logger.getLogger(className);
		LOGGER.log(Level.INFO, message);
	}
	
	public static void error(String className, String message) {
		LOGGER = Logger.getLogger(className);
		LOGGER.log(Level.SEVERE, message);
	}
	
	public static void debug(String className, String message) {
		LOGGER = Logger.getLogger(className);
		LOGGER.log(Level.WARNING, message);
	}
}
