package windows;

import res.Dimensions;
import res.Strings;

import logs.DLog;
import database.DatabaseConnection;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;

public class Login extends Application{
	
	private AnchorPane root;
	private Scene scene;
	private VBox vinformation;
	
	// Information of the user
	private Label luser;
	private TextField user;
	private HBox huser;
	private Label lpass;
	private PasswordField pass;
	private HBox hpass;
	
	// Buttons
	private HBox hbuttons;
	private Button biniciar;
	private Button bsalir;
	
	private void initialize_components() {
		root = new AnchorPane();
		vinformation = new VBox();
		scene = new Scene(root, Dimensions.WIDTH_LOGIN, Dimensions.HEIGTH_LOGIN);
		
		luser = new Label(Strings.user_label);
		user = new TextField();
		huser = new HBox();
		
		lpass = new Label(Strings.password_label);
		pass = new PasswordField();
		hpass = new HBox();
		
		biniciar = new Button(Strings.login_button);
		bsalir = new Button(Strings.exit_button);
		hbuttons = new HBox();
		
		manageHBox();
	}
	
	private void manageHBox() {
		try {
			huser.getChildren().addAll(luser, user);
			huser.setSpacing(Dimensions.SPACING_USER_INFO);
			
			hpass.getChildren().addAll(lpass, pass);
			hpass.setSpacing(Dimensions.SPACING_PASS_INFO);
			
			hbuttons.getChildren().addAll(biniciar, bsalir);
			hbuttons.setSpacing(Dimensions.SPACING_BUTTONS_LOGIN);
			
			vinformation.getChildren().addAll(huser, hpass, hbuttons);
			vinformation.setSpacing(Dimensions.SPACING_VBOX);
		} catch (Exception e) {
			DLog.error(this.toString(), "Error to created the HBox");
		}
	}
	
	private void settings_buttons(Stage stage) {
		biniciar.setOnAction(e -> {
			if (isNotEmpty() && DatabaseConnection.validate_user(user.getText(), pass.getText())) {
				Home goToHome = new Home();
				goToHome.start(stage);
			} else System.out.println("Fallo en el ingreso de login");;
		});
		
		bsalir.setOnAction(e -> stage.close());
	}
	
	private boolean isNotEmpty() {
		if(!user.getText().isEmpty() || !pass.getText().isEmpty()) return true;
		return false;
	}
	
	@Override
	public void start(Stage stage) {
		initialize_components();
		settings_buttons(stage);
		
		root.getChildren().add(vinformation);
		
		stage.setScene(scene);
		stage.setTitle(Dimensions.TITLE_LOGIN);
		stage.centerOnScreen();
		stage.setResizable(false);
		stage.show();
	}
	
	// Principal method to upload the page
	public static void main(String[] args) {
		launch(args);
	}
}
