package windows;

import res.Dimensions;
import res.Components;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

import javafx.scene.control.MenuBar;

import javafx.scene.layout.AnchorPane;

public class Home extends Application{
	
	private AnchorPane root;
	private Scene scene;
	
	private final MenuBar menu = Components.getTopMenu(Dimensions.WIDTH_HOME);
	
	public Home() {
		root = new AnchorPane();
		root.getChildren().add(menu);
		
		scene = new Scene(root, Dimensions.WIDTH_HOME, Dimensions.HEIGTH_HOME);
	}
	
	@Override
	public void start(Stage stage) {
		stage.setScene(scene);
		stage.setTitle(Dimensions.TITLE_HOME);
		stage.setResizable(false);
		stage.centerOnScreen();
		stage.show();
	}
}
