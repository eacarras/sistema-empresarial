package dialogs;

import res.Strings;

import javafx.stage.StageStyle;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Warnings {
	
	public static void showWarningExit() {
		Alert alert_exit = new Alert(AlertType.WARNING);
		alert_exit.setTitle(Strings.warning_exit_title);
		alert_exit.setContentText(Strings.warning_exit_content);
		alert_exit.initStyle(StageStyle.UTILITY);
		alert_exit.showAndWait();
	}
	
}
