package res;

public class Strings {
	
	// String for the login
	public final static String user_label = "Usuario:";
	public final static String password_label = "Contrasena:";
	public final static String login_button = "Iniciar Sesion";
	public final static String exit_button = "Salir";
	
	// String for the Home class
	
	// String for the Dialogs
	public final static String warning_exit_title = "Cuidado! Usted esta a punto de salir";
	public final static String warning_exit_content = "Todo lo que no ha sido guardado no se podra recuperar";
	
	// String for the Components class
	public final static String menu_file = "Archivo";
	public final static String menu_item_exit_file = "Salir";
	public final static String menu_options = "Opciones";
	public final static String menu_item_edit_options = "Editar";
	public final static String menu_help = "Ayuda";
	public final static String menu_item_contact_help = "Contactenos";
}
