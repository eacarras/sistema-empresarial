package res;

import res.Strings;

import dialogs.Warnings;

import javafx.scene.control.MenuItem;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Menu;

public class Components {
	
	public static MenuBar getTopMenu(double width) {
		MenuBar menu = new MenuBar();
		menu.setPrefWidth(width);
		
		Menu marchivo = new Menu(Strings.menu_file);
		MenuItem msalir = new MenuItem(Strings.menu_item_exit_file);
		msalir.setOnAction(e -> Warnings.showWarningExit());
		marchivo.getItems().addAll(msalir);
		
		Menu mopciones = new Menu(Strings.menu_options);
		MenuItem medit = new MenuItem(Strings.menu_item_edit_options);
		mopciones.getItems().addAll(medit);
		
		Menu mhelp = new Menu(Strings.menu_help);
		MenuItem mcontactenos = new MenuItem(Strings.menu_item_contact_help);
		mhelp.getItems().addAll(mcontactenos);
		
		menu.getMenus().addAll(marchivo, mopciones, mhelp);
		return menu;
	}
	
}
