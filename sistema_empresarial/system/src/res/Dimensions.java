package res;

public class Dimensions {
	
	// String values only for titles
	public final static String TITLE_LOGIN = "Bienvenido Loggeate para comenzar";
	public final static String TITLE_HOME = "Bienvenido al Sistema";
	
	// Double values
	// Login values
	public final static double WIDTH_LOGIN = 700.0;
	public final static double HEIGTH_LOGIN = 400.0;
	public final static double SPACING_USER_INFO = 35.0;
	public final static double SPACING_PASS_INFO = 10.0;
	public final static double SPACING_BUTTONS_LOGIN = 10.0;
	public final static double SPACING_VBOX = 10.0;
	
	// Home values
	public final static double WIDTH_HOME = 900.0;
	public final static double HEIGTH_HOME = 650.0;
}
