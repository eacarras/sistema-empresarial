package database;

import java.sql.*;

import java.util.Properties;

import logs.DLog;

public class DatabaseConnection {
	
	// Library, NameOfDatabase, Host and Port
    public static String driver = "com.mysql.cj.jdbc.Driver";
    public static String database = "system";
    public static String hostname = "localhost";

    // Path
    public static String url = "jdbc:mysql://" + hostname + "/" + database;

    private static Connection conectarMySQL() {
    	Connection conn = null;
        Properties proper = new Properties();
        proper.put("user", "root");
        proper.put("password", "aaron1624");

        try {
        	Class.forName(driver);
            conn = DriverManager.getConnection(url, proper);
        } catch (Exception e) {
        	DLog.error("Database Connection", "Problemas con la base de datos " + e);
        }
            
        return conn;
    }
    
    public static boolean validate_user(String user, String pass) {
    	DLog.info("Database Connection", "Validando informacion de usuario ");

        Connection db = null;
        Statement st = null;
        ResultSet rs = null;

        try {
                db = conectarMySQL();
                st = db.createStatement();               
                rs = st.executeQuery("SELECT * FROM user;");

                while (rs.next()) { 
                        if(user.equals(rs.getString("id")) && pass.equals(rs.getString("pass"))) return true;                 
                }
                return false;
        } catch (Exception e) {
                System.out.println("Error en el query " + e);
        }
        
        return false;
    }
}
